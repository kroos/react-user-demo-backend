create table users(
   id bigint auto_increment not null,
   name varchar(200) null,
   username varchar(200) null,
   email varchar(200) null,
   primary key(id)
);

insert into users(name, username, email)
values('longsiwei', 'long', 'long@123.com');
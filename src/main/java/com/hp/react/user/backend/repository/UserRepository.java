package com.hp.react.user.backend.repository;

import com.hp.react.user.backend.domain.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by longsiwei on 17/12/28.
 */
@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {

}

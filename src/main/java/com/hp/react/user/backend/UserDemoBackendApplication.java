package com.hp.react.user.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserDemoBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserDemoBackendApplication.class, args);
	}
}

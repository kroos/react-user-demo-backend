package com.hp.react.user.backend.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hp.react.user.backend.domain.User;
import com.hp.react.user.backend.repository.UserRepository;
import com.hp.react.user.backend.util.Const;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

/**
 * Created by longsiwei on 17/12/28.
 */
@RestController
@RequestMapping("/users")
public class UserService {

    @Autowired
    private UserRepository userRepository;
    //@Autowired
    //private BCryptPasswordEncoder passwordEncoder;

    @RequestMapping("/test")
    public String hello(){
        return "hello";
    }

    @RequestMapping("/page/list/{page}")
    public Page<User> getPageList(@PathVariable int page){
        Pageable pageable = new PageRequest(page - 1, Const.DEFAULT_NUM_PER_PAGE, Sort.Direction.DESC, "id");
        return userRepository.findAll(pageable);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/create")
    public User saveUser(@RequestBody String body){
        System.out.println("body: " + body);

        Gson gson = new GsonBuilder().create();
        User user = gson.fromJson(body, User.class);

        userRepository.save(user);

        return user;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{id}")
    public String deleteUser(@PathVariable String id){
        userRepository.delete(Long.valueOf(id));

        return id;
    }
}

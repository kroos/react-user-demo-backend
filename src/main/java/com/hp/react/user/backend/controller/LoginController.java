package com.hp.react.user.backend.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hp.react.user.backend.domain.User;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by longsiwei on 18/1/22.
 */
@RestController
public class LoginController {

    @PostMapping("/login")
    public String doLogin(@RequestBody String body){
        System.out.println(body);
        return body;
    }
}
